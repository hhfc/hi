FROM ubuntu:20.04

# Install git
RUN apt-get update && apt-get install -y git

# Clone Invidious repo
RUN git clone https://github.com/iv-org/invidious.git

# Go into repo directory
WORKDIR /invidious

# Run docker-compose
RUN docker-compose up